#!/bin/bash

if [ ${TOKEN} ] ; then
	mkdir "/etc/asn/"
	echo "${TOKEN}" > "/etc/asn/iqs_token"
	chmod -R 700 "/etc/asn/"
	chown -R nobody /etc/asn/
fi

/app/asn -l 0.0.0.0
