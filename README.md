# ASN Docker

[ASN by Nitefood](https://github.com/nitefood/asn) as a Docker container, exposing the Web API on Port 49200. Set the environment variable ```TOKEN``` to your IPQualityScore API Key for extended reputation lookups.
