FROM alpine:latest AS builddeps
ENV GREPCIDRVERSION=2.0
RUN apk add --no-cache gcc curl make musl-dev
RUN curl -L "http://www.pc-tools.net/files/unix/grepcidr-${GREPCIDRVERSION}.tar.gz" | tar -zx -C /tmp
WORKDIR /tmp/grepcidr-${GREPCIDRVERSION}
RUN make
RUN chmod 755 grepcidr
RUN mv grepcidr /

FROM builddeps AS install
RUN mkdir -p /app
RUN apk add --no-cache curl
RUN curl "https://raw.githubusercontent.com/nitefood/asn/master/asn" > /app/asn
RUN chmod 0755 /app/asn

FROM alpine:latest
ENV TOKEN=""

RUN apk update && apk add --no-cache curl whois mtr jq ipcalc nmap-ncat aha bind-tools bash nmap ncurses
COPY --from=install /app /app
COPY --from=builddeps /grepcidr /usr/bin/grepcidr
COPY entry.sh /

EXPOSE 49200/tcp
ENTRYPOINT bash /entry.sh
